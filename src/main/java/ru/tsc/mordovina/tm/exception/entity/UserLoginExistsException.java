package ru.tsc.mordovina.tm.exception.entity;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(final String login) {
        super("Error. User with login `" + login + "` already exists.");
    }

}
