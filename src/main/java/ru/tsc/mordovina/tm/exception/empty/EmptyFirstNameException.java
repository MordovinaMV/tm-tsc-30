package ru.tsc.mordovina.tm.exception.empty;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error.First name is empty.");
    }

}
