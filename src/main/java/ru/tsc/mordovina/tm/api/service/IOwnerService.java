package ru.tsc.mordovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.IService;
import ru.tsc.mordovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    @NotNull
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    void remove(@NotNull String userId, @Nullable E entity);

    @NotNull
    Integer getSize(@NotNull String userId);

}
