package ru.tsc.mordovina.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.mordovina.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}