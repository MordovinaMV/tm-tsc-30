package ru.tsc.mordovina.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.mordovina.tm.component.Bootstrap;
import ru.tsc.mordovina.tm.util.SystemUtil;

public class Application {

    public static void main(String[] args) {
        System.out.println("PID: " + SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}